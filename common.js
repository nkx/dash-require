let main = require('../build/index');
module.exports = function (p, name) {
  if(!name) {
    return require(p);
  }
  Object.defineProperty(this, name, {
    get: () => { return require(p) }
  }
}