/**
 * Module lib
 * map modules/classes to their pendant within the scope object.
 * so you can select the module ./foo/bar.js easily via scope.foo.bar
 * the module only gets loaded with required, 
 * when you select it for the first time
 * @example // initialize with global options
 *          let requireLib = require('dash-require')({
 *          root: './lib',
 *          });
 *          // load various files
 *          // first the modules, than the classes 
 *          // you can use a object as last argument,
 *          // to directly map it within. 
 *          let pack=requireLib(
 *          '/app/init-collection.js', // modules are stored with camelcase
 *          '/app/models/*' // classes are stored with pascalcase
 *          );
 *          // => {app:{
 *          //      initCollection: [GETTER]
 *          //     }, models: {ModelClassA:[GETTER]}}
 * @module dash-require
 */

const _requireDir = require('./lib/require-dir');
const path = require('path');
//const fs = require('fs');
const {camelcase,toType,pascalcase} = require('./lib/utils');
/*
var toType = function (obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}*/

const [requireLib,requireCls] = [
  // require library files  (camelCase)
  (path, opt, ...args) => _requireDir(path, Object.assign({
    filecase: 'camelcase'
  }, opt), ...args),
  // require class files  (PascalCase)
  (path, opt, ...args) => _requireDir(path, Object.assign({
    filecase: 'pascalcase'
    
  }, opt), ...args)
];
/**
 * library loader
 * @private
 * @param   {mixed(array|object)} classes class pathes to load
 * @param   {mixed(array|object)} lib     module pathes to load
 * @param   {object}              opt     options
 * @returns {object}              scope object containing the loaded classes/modules
 */
function _include(classes, lib, opt) {
  if(classes instanceof Object) {
    opt = classes
    classes = opt.classes
    lib = opt.modules
  } else if(lib instanceof Object) {
    opt=lib; lib=opt.modules;
  }
  opt = Object.assign({}, this._options || {}, opt || {});
  let scope = (opt.scope || this._scope || {});
  try {
    if(opt.autoResolve) {
      _requireDir(classes, Object.assign({
        rename: (key,file) => {
          let abspath=path.resolve(opt.cwd||process.cwd(), file);
          try {
            const res = require(abspath);
            if(res instanceof Function && res.name && res.prototype instanceof Object ) {
              // class name
              return pascalcase(key);
            } else {
              return camelcase(key);
            }
            //delete require._cache[abspath];
          } catch(e) {
            // when module could not be loaded,
            // we tell the caller to remove the key from the scope
            return false; 
          }
        }
      },opt), scope);
      
      return scope;
    }
    if (classes) {
      requireCls(classes, Object.assign(opt, opt.cls || opt.classesOption), scope);
      
    }
    if (lib) {
      requireLib(lib, Object.assign(opt, opt.md || opt.lib || opt.modulesOption), scope);
    }
  } catch(e){
    throw new Error('[dash-require] '+e.message);

  }
  return scope;
}
/**
 * module implementation
 * 
 * @param   {object} opt     options
 * @param   {mixed}  ...args may contain classes/modules or the target scope
 *                             @see _include  
 * @returns {object} scope object containing the loaded classes/modules
 */
function _init(...args) {
  this._options = {}
  let lib, classes, opt, scope = {};

  args.forEach((arg)=> {
    if(toType(arg)==='string' || toType(arg)==='array' || arg===null){
      if(classes===undefined) classes=arg 
      else lib=arg;
    } else if(toType(arg)==='object') {
      if(!opt) opt=arg 
      else scope=arg;
    }
  });
  opt= (opt || {});
  opt.scope = scope;
  classes = (classes || opt.classes)
  lib = (lib || opt.lib)
  if (!classes && !lib) {
    this._options = opt;
    this._scope = scope;
    return _include.bind(this);
  }
  return _include(classes, lib, opt);;
}
module.exports = function Foo(scope, name, opt) {
  if(scope instanceof module.constructor) {
    /*let parent=scope;
    while(parent.parent) {
      parent = parent.parent;
    }*/
    opt=Object.assign({cwd:path.dirname(scope.filename)},opt||{})
    return _init.apply(this, [opt, scope.exports[name]={}]);
  }
  return _init.apply(this, arguments)
}
module.exports.requireLib = requireLib
module.exports.requireCls = requireCls

