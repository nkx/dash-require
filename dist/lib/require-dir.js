/**

The MIT License (MIT)

Copyright (c) 2017 nk <nk@k46.net>
Copyright (c) 2011 Troy Goode <troygoode@gmail.com>

*/

'use strict';

const fs = require('fs'),
  globby = require('globby'),
  {
    camelcase,
    pascalcase
  } = require('./utils'),

  {
    join,
    //resolve,
    basename,
    dirname
  } = require('path'),
  defaultOptions = {
    extensions: ['js', 'json', 'coffee'],
    recurse: true,
    camelcase: true, // use camelCase for submodule-/file- names
    pascalcase: false, // use PascalCase for submodule-/file- names
    filecase: undefined, // the case only for files (camelcase|pascalcase)
    rename: function (name) {
      return name;
    },
    renameScope: function (name) {
      return name;
    },
    visit: function (obj) {
      return obj;
    },
    //cwd: resolve(__dirname, '..', '..'),
    cwd: process.cwd(),
    root: undefined
  };


/**
 * runs the globby module
 * @private
 * @param   {mixed(string|aray)} pat the file match pattern
 * @param   {object}             opt = defaultOptions globby configuration
 * @returns {array}              the resolved pathes
 */
function _resolveGlob(pat, opt = defaultOptions) {
  return globby.sync(pat, opt)
}

function _parseScope(item, scope, cb) {
  let res = (cb && cb())
  
  item = (res === undefined) && item || res;
  if (scope !== undefined && item) {
    return scope[item] = scope[item] || {};
  }
}

function _parsePaths(filepath, options, scope = {}) {
let tmpscope = scope;
  let filename = basename(filepath),
    filepath_noext = filepath.substr(0, filepath.lastIndexOf('.'));
  if (fs.statSync(join(options.cwd, filepath)).isDirectory()) {
    // @todo useless?
    if (!options.root) {
      //retval = retval[options.rename(filename, filepath)] = {};
    }
  } else {
    // remove the file extension so node.require get used in the wright way
    let key = filename.substring(0, filename.lastIndexOf('.'));
    if (options.uppercamelcase || options.pascalcase) {
      key = pascalcase(key); // PascalFileName
    } else if (options.camelcase) {
      key = camelcase(key); // camelFileName
    }
    if(options.flat) {
      key = filepath.replace(filename, key);
      let _deli = options.flatDelimiter
      if(!_deli && !(options.flat instanceof Boolean)) {
        _deli = options.flat;
      }
      if(_deli) {
        key=key.replace(/\//g,_deli);
      }
    }
    if (options.rename) {
      let tkey = options.rename(key, filepath, scope);
      if (tkey === null || tkey === false) {
        return;
      } else if (tkey !== undefined) {
        key = tkey;
      }
    }
    if (options.root) {
      // when defined, replace the root dir of the current path,
      // and use the remaining directories of the path as scope
      let rgx = new RegExp('^\.?' + options.root + '(?:/(.*)/)?.[^/]+$');
      if (rgx.test(filepath)) {
        // use the directories as scope
        let tfilepath = filepath.replace(rgx, '$1');
        //tfilepath.split('/').forEach((item) => scope=_parseScope(item, key, options, tmpscope));
        //if(tfilepath) {
        //console.log('tfilepath', key, tfilepath, options.root);
        if(tfilepath)
          tfilepath.split('/').forEach((item) => scope = _parseScope(item, tmpscope, () => options.renameScope(item, key, filepath, tmpscope)));
        //}
      }
    } else if(!options.flat) {
      /*dirname(filepath).split('/').forEach((item) => {
        scope = _parseScope(item, scope, () => options.renameScope(item, key, filepath, scope))
      });*/
      dirname(filepath).split('/').forEach((item) => {
          if (item && item !== '.') {
            let res = (options.renameScope && options.renameScope(item, key, filepath, scope));
              item =(res === null) && item || res;
              if (item) {
                scope = scope[item] = tmpscope[item] || {};
              }
          }
        });
    }
    if (key in scope) {
      console.warn(`require-dir: scope allready has a property named "${key}".`)
      return;
    }
    // finally prepare the require loading.
    // therefor we store the action as property with in the current scope.
    // ensure that we have the fullpath to the file
    let _path = join(options.cwd, filepath_noext);
    if (!Object.hasOwnProperty(scope, key)) {
      Object.defineProperty(scope, key, {
        enumerable: true,
        get: () => {
          try {
            //console.log('_path', _path);
            return require(_path);
          } catch (e) {
            if (!options.autoResolve) {
              throw new Error(e);
            }
          }
        }
      });
    } else {
      console.log('Allready has property', key);  

    }
  }

}
/**
 * load a directory to map files from
 * @param   {string} path         string
 * @param   {object} [options={}] options containing also configuration for globby
 * @returns {object} the result object should contain the loaded files.
 *                                if not defined otherwise, the name of the directory containing the files, will be used as object key for subobject 
 */
function requireDir(path, options, scope) {
  var tmpscope = scope || {};
  if (path instanceof Object && !(path instanceof Array)) {
    let props = path;
    path = [];
    Object.getOwnPropertyNames(props).forEach((key) => {
      if (/[$_.:]/.test(key)) {
        path.push(props[key]);
        //requireDir(props[key], options, tmpscope);
      } else {
        tmpscope[key] = requireDir(props[key], options, tmpscope[key]);
      }
    });
  }
  // path is optional
  // default options
  options = options || {};
  for (var prop in defaultOptions) {
    if (typeof options[prop] === 'undefined') {
      options[prop] = defaultOptions[prop];
    }
  }
  if (options.filecase)
    options[options.filecase] = true;
  if (options.root) {
    // ensure that the root path is with in the cwd
    //options.root=resolve(options.cwd,options.root);
    // because of the absolute path of root, we have to disable globby.absolute 
    options.absolute = false;
  }

  // get the file pathes
  path = _resolveGlob(path, {
    cwd: options.cwd,
    root: options.root,
    absolute: options.absolute,
    nomount: options.nomount,
  })
  path.forEach((filepath) => {
    _parsePaths(filepath, options, tmpscope);
  });
  
  return tmpscope;
}

module.exports = requireDir;
module.exports.defaults = defaultOptions;
