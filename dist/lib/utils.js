const utils = exports = {
  toType(obj) {
    return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
  },
  camelcase(str) {
    return str.replace(/^([A-Z])|[\s-_](\w)/g, function(match, p1, p2) {
        if (p2) return p2.toUpperCase();
        return p1.toLowerCase();        
    });
  },
  pascalcase(str) {
    return (str = utils.camelcase(str)) && str[0].toUpperCase() + str.substr(1);
  }
}
module.exports = utils;
