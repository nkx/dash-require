//
// Basic Requiring
// opt.cwd - current directory to search for files
//
let _require = require('../src/index')({cwd:process.cwd()+'/test/fixtures'});

//
// require app as class and all with in lib as library
//
console.info('// require app as class and all with in lib as librarie');
let package = _require('app.js','lib/**/*.js'),
    app = package.App,
    utils = package.lib.utils;
console.log('App typeof', typeof(app), '\nUtils typeof', typeof(utils));

//
// option lib.root
//
console.info('// option lib.root');
let src = _require('app.js','lib/**/*.js',{
  lib:{root:'lib'}, // with this option the directory "lib" will not land in the scope
  scope: {} // reset the current scope
});
console.log(src);

//
// option lib.rename
//
console.info('// option lib.rename');
src = _require('app.js','lib/**/*.js',{
  rename: (key , path) => (key==='App' && 'MyApp'), // remove lib from scope
  scope: {} // reset the current scope
});
console.log(src);

//
// option lib.renameScope
//
console.info('// option lib.renameScope');
src = _require('app.js','lib/**/*.js',{
  renameScope: (dir, key , path) => (dir!=='lib' && dir), // remove lib from scope
  scope: {} // reset the current scope
});
console.log(src);
