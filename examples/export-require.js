const path = require('path')
const util = require('util')
//
// Basic Requiring
// opt.cwd - current directory to search for files
//
const _require = require('../src/index')(module, 'app', {
  autoResolve: true
});
_require('../{src,test}/**/*.js');


let ctxmap = new Map();
let filepath = [
  '/home/joe/bar/test/bv/.config',
  '/joe/home/joe/.local/.foo',
  '/usr/bin/local/share/var.s',
  '/usr/bin/share/file.txt',
  '/usr/sbin/node.js',
  '/home/joe/.local/npm/foo.js',
  ];

function* gen() {
  let _name, level = 1;
  let mapEntries = ctxmap.entries();
  let parent, done = false;
  do {
    parent = _name;
    [_name, _index, _path] = yield;
    if (_index === 1) {
      parent = undefined
      level = 0
    };
    //yield _name
    level = yield* map(_name, Number(level), parent, done);
  } while (_name !== undefined);

}

function* map(name, level, p, done) {
  //parent = [...ctxmap.keys()].filter((value) => value.level==(level-1) )
  //ctxmap.

  let o = {
    name: name,
  };
  let itemkey = {
    level
  }
  let parent;
  if (p) {
    [...ctxmap.entries()].reverse().forEach(([key, value]) => {
      
      if (value.name === p && key.level=== level-1) {
        //plevel = key.level;
        o.parent = ctxmap.get(key)
          //console.log('key.level', key.level, o.parent);
        o.parent.add(itemkey);
        return;
      }
    });
    //if (done) return;
  }
    //o.parent = 1
    Object.defineProperties(o, {
      
      add: {
      enumerable: false,
      configurable: true,
      value: function(item) {
        o.subdirs.push(item);
      },
    },
      subdirs: {
        enumerable: true,
        configurable: true,
        value: [],
      },
      parent: {
        enumerable: false,
        configurable: true,
        get: () => o.parent || null
      },
    });
  //(level) && (o.pl = level-1)
  ctxmap.set(itemkey, o);
  return level + 1;
}
let dirclrt = gen(ctxmap),
  depth, res;
filepath.forEach((fp) => {

  let _stdout = [fp, '\t'];
  depth = 0;
  let replfilepath = fp.replace(/(?:[a-z.]+|\b\w|\w$)/g, (...args) => dirclrt.next.call(dirclrt, args) && '');


});

ctxmap = new Map([...ctxmap].sort(([a], [b]) => {
  return b.level - a.level;
}))
let entries = ctxmap.entries(),
  key;
for (let item of entries) {
  let [k, v] = item;
}
let items =[...ctxmap.entries()].reverse();

items.forEach(()=>{
  
})
console.log('items', util.inspect(items, 2,"-"));



//require('@index').export();
//console.log(`"examples/${ path.basename(module.filename) }" :`, util.inspect(module.exports.app['..'].src,true));
