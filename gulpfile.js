'use strict';

const gulp = require('gulp'),
      jasmine = require('gulp-jasmine'),
      gulpIf = require('gulp-if'),
      changed = require('gulp-changed'),
      clean = require('gulp-clean'),
      bump = require('gulp-bump'),
      jsonTransform = require('gulp-json-transform'),
       eslint = require('gulp-eslint');

const argv = require('yargs')
    .count('verbose')
    .alias('v', 'verbose')
    .argv;


const SRC = ['src/**/*.js','!**/_*.js','!node_modules/**'];
const DEST = './dist'


gulp.task('lint', function () {
  gulp.src(SRC)
    //.pipe(changed(DEST))
    //.pipe(DEST)
    .pipe(eslint({
			fix: true
		}))
    .pipe(eslint.format())
		//.pipe(gulp.dest(DEST));
		//.pipe(gulp.dest(DEST));
    .pipe(eslint.failAfterError());
});
function isFixed(file) {
	// Has ESLint fixed the file contents?
	return file.eslint != null && file.eslint.fixed;
}
gulp.task('lint-n-fix', () => {

	return gulp.src(SRC)
		.pipe()
		.pipe(eslint.format())
		// if fixed, write the file to dest
		.pipe(gulpIf(isFixed, gulp.dest('./tmp')));
});
gulp.task('dev', () => {
  
	return gulp.src('examples/b')
		.pipe()
		.pipe(eslint.format())
		// if fixed, write the file to dest
		.pipe(gulpIf(isFixed, gulp.dest('./tmp')));
});
gulp.task('test', function () {
  let jasmineConfig=require('./test/support/jasmine.json');
  if(!argv.v){
  delete jasmineConfig.spec_files;
  }
  gulp.src('./test/spec/**.js')
    .pipe(jasmine({
      config: jasmineConfig,
      errorOnFail: true,
    }))
    /*.on('jasmineDone', function (allfine) {
      if(allfine) {
        console.log('arguments', arguments);
      }
    });*/
});

gulp.task('bump', function(){
  var options = {
    type: 'minor'
  };
  gulp.src('./package.json')
  .pipe(bump(options))
  .pipe(gulp.dest(DEST));
});
gulp.task('patch', function(){
  gulp.src('./package.json')
  .pipe(bump())
  .pipe(gulp.dest(DEST));
});

gulp.task('clean', function(){
  return gulp.src('./{tmp,build,dist}', {read: false})
        .pipe(clean());
});
gulp.task('build', function(){
  //let pkg = require('./package.json');
  gulp.src(SRC)
    /*.pipe(jsonTransform(function(data, file) {
      delete data.devDependencies;
      data.main ='index.js';
      return data;
    }))*/
  //.pipe(bump())
  .pipe(gulp.dest(DEST));
});

gulp.task('default', ['clean','lint','build'], function () {
  //console.log("Args:",arguments)
});
