/**
 * ___loading and mapping of "modules" and "classes"___
 *
 * map classes/modules to their pendant by creating a *nested* or *flatten* object.
 * - "classes" by default are mapped with an clean name using pascalcase (app.js => App)
 * - "modules" by default using camelcase type for the name (lib/my-utils.js => lib.MyUtils)
 *
 * so you can select the module ./foo/bar.js easily via scope.foo.bar
 * the module only gets loaded with required, 
 * when you select it for the first time
 *
 * @module dash-require
 *
 * @example <caption>directory structure</caption>
 * src/devices/keyboard/keymap.js
 * src/devices/keyboard/__keyboard.js
 * src/devices/keyboard/index.js
 * src/lib/__device.js
 * src/lib/__keyboard-device.js
 * src/lib/abstract-event.js
 * src/lib/abstract-device.js
 * src/lib/midi-event.js
 * src/lib/midi-device.js
 * @example
 * // first we define some global options,
 * const _require = require('dash-require')({
 *   // set the base directory
 *   cwd: './src',
 *   // get rid of some directories
 *   renameScope: (name) => (name!=='lib' && name!=='keyboard') && name,
 *   // use the `cls` options, restricted to the class files,
 *   // to beautify the devices result a little bit,
 *   // so it uses the directory name instead of 'Index'
 *   cls: {
 *     rename: (name, path, scope) => {
 *       if(path.indexOf('devices/')!==-1)
 *         return path.replace(/^devices\/(.*)\/.+$/,'$1');
 *     }
 *   }
 * });
 * // now we can use the returned function to include our files
 * // with the first argument we import files as classes.
 * module.exports = _require(
 *     '{lib,devices/**}/[!_]*.js',
 *     'lib/{terminal,utils}.js'
 * );
 * @example <caption>returns</caption>
 *  {
 *    devices: {keyboard: [Getter]},
 *    AbstractDevice: [Getter],
 *    AbstractEvent: [Getter],
 *    MidiDevice: [Getter],
 *    MidiEvent: [Getter],
 *  }
 */



const _requireDir = require('./lib/require-dir');
const path = require('path');
//const fs = require('fs');
const {
  camelcase,
  toType,
  pascalcase
} = require('./lib/utils');

const [requireLib, requireCls] = [
  // require library files  (camelCase)
  (path, opt, ...args) => _requireDir(path, Object.assign({
    filecase: 'camelcase'
  }, opt), ...args),
  // require class files  (PascalCase)
  (path, opt, ...args) => _requireDir(path, Object.assign({
    filecase: 'pascalcase'

  }, opt), ...args)
];
/**
 * library loader
 * @private
 * @param   {mixed(array|object)} classes class pathes to load
 * @param   {mixed(array|object)} lib     module pathes to load
 * @param   {object}              opt     options
 * @returns {object}              scope object containing the loaded classes/modules
 */
function _include(classes, lib, opt) {
  if (classes instanceof Object) {
    opt = classes
    classes = opt.classes
    lib = opt.modules
  } else if (lib instanceof Object) {
    opt = lib;
    lib = opt.modules;
  }
  opt = Object.assign({}, this._options || {}, opt || {});
  let scope = (opt.scope || this._scope || {});
  try {
    if (opt.autoResolve) {
      _requireDir(classes, Object.assign({
        rename: (key, file) => {
          let abspath = path.resolve(opt.cwd || process.cwd(), file);
          try {
            const res = require(abspath);
            if (res instanceof Function && res.name && res.prototype instanceof Object) {
              // class name
              return pascalcase(key);
            } else {
              return camelcase(key);
            }
            //delete require._cache[abspath];
          } catch (e) {
            // when module could not be loaded,
            // we tell the caller to remove the key from the scope
            return false;
          }
        }
      }, opt), scope);

      return scope;
    }
    if (classes) {
      requireCls(classes, Object.assign(opt, opt.cls || opt.classesOption), scope);

    }
    if (lib) {
      requireLib(lib, Object.assign(opt, opt.md || opt.lib || opt.modulesOption), scope);
    }
  } catch (e) {
    throw new Error('[dash-require] ' + e.message);


  }
  return scope;

}
/**
 * @private
 * @see __include
 */
function _init(...args) {
  this._options = {}
  let lib, classes, opt, scope = {};

  args.forEach((arg) => {
    if (toType(arg) === 'string' || toType(arg) === 'array' || arg === null) {
      if (classes === undefined) classes = arg
      else lib = arg;
    } else if (toType(arg) === 'object') {
      if (!opt) opt = arg
      else scope = arg;
    }
  });
  opt = (opt || {});
  opt.scope = scope;
  classes = (classes || opt.classes)
  lib = (lib || opt.lib)
  if (!classes && !lib) {
    this._options = opt;
    this._scope = scope;
    return _include.bind(this);
  }
  return _include(classes, lib, opt);;
}

/**
 * Initialize
 * @member {function}
 * @memberof dash-require
 * @returns {object} module - contains the loaded classes/modules
 */
module.exports = function Foo(scope, name, opt) {
  if (scope instanceof module.constructor) {
    /*let parent=scope;
    while(parent.parent) {
      parent = parent.parent;
    }*/
    opt = Object.assign({
      cwd: path.dirname(scope.filename)
    }, opt || {})
    return _init.apply(this, [opt, scope.exports[name] = {}]);
  }
  return _init.apply(this, arguments)
}
/**
 * includes file as a module/library
 * uses `opt.filecase: camelcase`
 * @see requireDir
 * @member {function}
 * @memberof dash-require.exports
 */
module.exports.requireLib = requireLib

/**
 * load and map file as a class
 * uses `opt.filecase: pascalcase`
 * @see requireDir
 * @member {function}
 * @memberof dash-require.exports
 */
module.exports.requireCls = requireCls
