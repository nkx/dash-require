const requireDir = require('./require-dir');
module.exports.requireCls = function(path,opt,...args){
  return requireDir(path, Object.assign({
    filecase:'pascalcase', 
  },opt),...args);
};