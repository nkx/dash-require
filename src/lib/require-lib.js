
const requireDir = require('./require-dir');
module.exports.requireLib = function(path,opt,...args){
  return requireDir(path, Object.assign({
    filecase:'camelcase', 
  },opt), ...args);
};
