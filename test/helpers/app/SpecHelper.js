const path = require('path');
beforeEach(function () {
  jasmine.addMatchers({
    toBeFixture: function () {
      return {
        compare: function (actual, expected) {
          expected = path.join('test','fixtures',expected);
          expected=require(path.resolve(expected));
          return {
            pass: expected === actual
          }
        }
      };
    }
  });
});
