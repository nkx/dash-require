//const jasmine                      = require("jasmine");

//jasmine.getEnv().addReporter(customReporter);
var myReporter = {
  jasmineStarted: function (suiteInfo) {
    console.log('Running suite with ' + suiteInfo.totalSpecsDefined);
  },
  suiteStarted: function (result) {
    console.log('Suite started: ' + result.description + ' whose full description is: ' + result.fullName);
  },
  specStarted: function (result) {
    console.log('Spec started: ' + result.description + ' whose full description is: ' + result.fullName);
  },
  specDone: function (result) {
    console.log('Spec: ' + result.description + ' was ' + result.status);
    for (var i = 0; i < result.failedExpectations.length; i++) {
      console.log('Failure: ' + result.failedExpectations[i].message);
      console.log(result.failedExpectations[i].stack);
    }
    console.log(result.passedExpectations.length);
  },
  suiteDone: function (result) {
    console.log('Suite: ' + result.description + ' was ' + result.status);
    for (var i = 0; i < result.failedExpectations.length; i++) {
      console.log('AfterAll ' + result.failedExpectations[i].message);
      console.log(result.failedExpectations[i].stack);
    }
  },
  jasmineDone: function () {
    console.log('Finished suite');
  }
}


const JasmineConsoleReporter = require('jasmine-console-reporter');
const SpecReporter = require('jasmine-spec-reporter').SpecReporter;
jasmine.getEnv().clearReporters(); // remove default reporter logs
let jasmineReporter = () => new JasmineConsoleReporter({ // add jasmine-spec-reporter
  colors: 1, // (0|false)|(1|true)|2 
  cleanStack: 3, // (0|false)|(1|true)|2|3 
  verbosity: 3, // (0|false)|1|2|(3|true)|4 
  listStyle: 'indent', // "flat"|"indent" 
  activity: false
});
let specReporter = () => new SpecReporter({ // add jasmine-spec-reporter
  suite: {
    displayNumber: true
  },
  spec: {
    displayPending: true,
    displayStacktrace: false,
    displayDuration: true,
    displaySuccessful: true,
    displayFailure: false,
  },
  summary: {
    displayPending: true,
    displayStacktrace: false,
    displayFailure: false,
    displaySuccessful: false,
    displayDuration: true,
  },
  prefix: {
    successfull: "llll "
  },
});
jasmine.getEnv().clearReporters(); // remove default reporter logs
jasmine.getEnv().addReporter(jasmineReporter());
