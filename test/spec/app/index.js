
var toType = function (obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}
describe("[app]", function () {
  const deps = require('../../common'),
        main = deps.main;
  let _require;
  
  beforeEach(function () {
    //{ main } = deps;
    
  });
  // initializing
  it("should initialize module, using test/fixtures as working directory", function () {
    _require = main({cwd:process.cwd()+'/test/fixtures'});
    expect(toType(_require)).toBe('function');
  });
  it("should assign class test/fixtures/app.js to scope.App", function () {
    let scope = _require('app.js','lib/*');
    expect(scope.App).toBeFixture('app');
    expect(scope.lib.utils).toBeFixture('lib/utils');
  });
  it("should assign module test/fixtures/lib/utils to scope.utils", function () {
    let scope = _require(null,'lib/**',{root:'lib',scope:{}});
    expect(scope.utils).toBeFixture('lib/utils');
  });
  
  
  /*
  // require files as classes (file-x.js => FileX)
  describe("when module is loaded", function() {
    it("should store the filename as CamelCased Property with in the lib-Scope.", function() {

        //expect(main).toEqual(true);
    });
  });
  var player;
  var song;
  beforeEach(function() {
    player = new Player();
    song = new Song();
  });
  describe("when song has been paused", function() {
    beforeEach(function() {
      player.play(song);
      player.pause();
    });
    it("should indicate that the song is currently paused", function() {
      expect(player.isPlaying).toBeFalsy();
      // demonstrates use of 'not' with a custom matcher
      expect(player).not.toBePlaying(song);
    });
    it("should be possible to resume", function() {
      player.resume();
      expect(player.isPlaying).toBeTruthy();
      expect(player.currentlyPlayingSong).toEqual(song);
    });
  });
  // demonstrates use of spies to intercept and test method calls
  it("tells the current song if the user has made it a favorite", function() {
    spyOn(song, 'persistFavoriteStatus');
    player.play(song);
    player.makeFavorite();
    expect(song.persistFavoriteStatus).toHaveBeenCalledWith(true);
  });
  //demonstrates use of expected exceptions
  describe("#resume", function() {
    it("should throw an exception if song is already playing", function() {
      player.play(song);
      expect(function() {
        player.resume();
      }).toThrowError("song is already playing");
    });
  });*/
});
