
var toType = function (obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}
describe("testing various options", function () {
  const deps = require('../../common'),
        main = deps.main;
  let _require;
  
  beforeEach(function () {
    //{ main } = deps;
    _require = main({cwd:process.cwd()+'/test/fixtures'});
  });
      it("option.classes\n\tresult should contain 'class' test/fixtures/app", function () {
        let scope = _require({classes:'app.js'});
        expect(scope.App).toBeFixture('app');
      });
      it("option.modules\n\tresult should now contain 'module' of directory test/fixtures/lib", function () {
        let scope = _require({modules:'lib/**'}); 
        expect(toType(scope.lib)).toBe('object');
        expect(scope.lib.utils).toBeFixture('lib/utils');
      });
      it("option.rename\n\tresult should rename 'class' test/fixtures/app to MyApp", function () {
        let scope = _require('app.js', {rename: (key , path) => (key==='App' && 'MyApp')});
        expect(scope.MyApp).toBeFixture('app');
      });
      it("option.renameScope\n\tshould store 'modules' from lib/* without using the directory as container with in the current scope", function () {
        let scope = _require(null, 'lib/**', {renameScope: (dir, key , path) => (dir!=='lib' && dir)});
        expect(scope.utils).toBeFixture('lib/utils');
        expect(scope.fs.readfile).toBeFixture('lib/fs/readfile');
      });
      it("option.root\n\tshould store 'modules' from lib/* without creating containers for the directories of the root path", function () {
        let scope = _require(null, 'lib/**', {root:'lib'});
        expect(scope.utils).toBeFixture('lib/utils');
        expect(scope.fs.readfile).toBeFixture('lib/fs/readfile');
      });
  
      it("option.autoResolve\n\tshould include files and automaticly store them as class or module.", function () {
        let scope=_require('**/*.js', {autoResolve: true, scope:{}});
        expect(scope.lib.utils).toBeFixture('lib/utils');
        expect(scope.lib.fs.readfile).toBeFixture('lib/fs/readfile');
        expect(scope.App).toBeFixture('app');
        expect(scope.AppCli).toBeFixture('app_cli');
      });

      it("option.flat\n\tshould build the scope by using a modified path of the current file as key (no scope nesting).", function () {
        let scope=main(null, 'lib/**/*.js', {cwd:process.cwd()+'/test/fixtures', flat:'_'});
        expect(scope.lib_utils).toBeFixture('lib/utils');
        expect(scope.lib_fs_readfile).toBeFixture('lib/fs/readfile');
      });

    describe("separate options for modules (option.md) and classes (option.cls)", function () {
      it("option.md\n\tshould build the scope by using a modified path of the current file as key (no scope nesting).", function () {
        let scope = _require('app.js','lib/**/*.js',{
          md:{root:'lib'}, // with this option the directory "lib" will not land in the scope
        });
        expect(scope.utils).toBeFixture('lib/utils');
        expect(scope.fs.readfile).toBeFixture('lib/fs/readfile');
      });
      it("option.filecase\n\tshould use camelcase for class names and pascalcase for modules.", function () {
        let scope = _require('app.js','lib/**/*.js',{
          cls:{filecase:'camelcase'}, 
          md:{filecase:'pascalcase'}, 
        });
        expect(scope.app).toBeFixture('app');
      });
    });
});
