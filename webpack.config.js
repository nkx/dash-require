var path = require('path');

module.exports = {
  entry: ['./index.js','lib/*.js'],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  }
};

